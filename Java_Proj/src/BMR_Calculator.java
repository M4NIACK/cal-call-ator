
public class BMR_Calculator {
	private double height, weight,bmr;
	private int age;
	private String gender;
	public BMR_Calculator(float height, float weight, int age, String gender) {
		this.height = height;
		this.weight = weight;
		this.age = age;
		this.gender = gender;
	}
	public double BMR_value()
	{
		if(gender.compareTo("Male")==0)
		{
			  bmr = 66.5 + (13.75 * weight) + (5.003 * height) - (6.75 * age);
			  return bmr;
		}
		else if(gender.compareTo("Female")==0)
		{
			bmr = 655.1 + (9.563 * weight) + (1.850 * height) - (4.676 * age);
		    return bmr;
		}
		return 0;
	}
	
}
