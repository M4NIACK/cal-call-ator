
public class Conversions {
	private String convert_from,convert_to;
	private double converted_value;
	
	public String getConvert_from() {
		return convert_from;
	}
	public void setConvert_from(String convert_from) {
		this.convert_from = convert_from;
	}
	public String getConvert_to() {
		return convert_to;
	}
	public void setConvert_to(String convert_to) {
		this.convert_to = convert_to;
	}
	public double getConverted_value() {
		return converted_value;
	}
	public void setConverted_value(double converted_value) {
		this.converted_value = converted_value;
	}
	public double convertlength(String convert_from, String convert_to, double converted_value)
	{
		if(convert_from.compareTo("Meter")==0)
		{
			switch(convert_to)
			{
			case "MicroMeter": converted_value = converted_value * 1000000;break;
			case "MiliMeter": converted_value = converted_value * 1000;break;
			case "CentiMeter": converted_value = converted_value * 100; break;
			case "Inch" : converted_value = converted_value * 39.3701;
			case "Feet" : converted_value = converted_value * 3.28084;break;
			case "KiloMeter": converted_value = converted_value * 0.001; break;
			case "Mile": converted_value = converted_value * 0.000621371; break;
			}
		}
		else if(convert_from.compareTo("KiloMeter")==0)
		{
			switch(convert_to)
			{
			case "Inch" : converted_value = converted_value * 39370.1; break;
			case "Feet" : converted_value = converted_value * 3280.84; break;
			case "Meter" : converted_value = converted_value * 1000; break;
			case "Mile": converted_value = converted_value * 0.621371; break;
			}
		}
		else if(convert_from.compareTo("CentiMeter")==0)
		{
			switch(convert_to)
			{
			case "MicroMeter" : converted_value = converted_value * 10000;
			case "MiliMeter" : converted_value = converted_value * 10;
			case "Inch" : converted_value = converted_value * 0.393701;
			case "DeciMeter" : converted_value = converted_value * 0.1;
			case "Feet" : converted_value = converted_value * 0.0328084;
			case "Meter" : converted_value = converted_value * 0.01;
			}
			
		}
		return converted_value;
	}
}
