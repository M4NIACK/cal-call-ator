package application;

public class Conversions {
	private String convert_from,convert_to;
	private double converted_value;
	public String getConvert_from() {
		return convert_from;
	}
	public void setConvert_from(String convert_from) {
		this.convert_from = convert_from;
	}
	public String getConvert_to() {
		return convert_to;
	}
	public void setConvert_to(String convert_to) {
		this.convert_to = convert_to;
	}
	public double getConverted_value() {
		return converted_value;
	}
	public void setConverted_value(double converted_value) {
		this.converted_value = converted_value;
	}
	public double convertlength(String convert_from, String convert_to, double converted_value)
	{
		if(convert_from.compareTo("Meter")==0)
		{
			switch(convert_to)
			{
			case "MicroMeter": converted_value = converted_value * 1000000;break;
			case "MiliMeter": converted_value = converted_value * 1000;break;
			case "CentiMeter": converted_value = converted_value * 100; break;
			case "Inch" : converted_value = converted_value * 39.3701; break;
			case "Feet" : converted_value = converted_value * 3.28084;break;
			case "KiloMeter": converted_value = converted_value * 0.001; break;
			case "Mile": converted_value = converted_value * 0.000621371; break;
			}
		}
		else if(convert_from.compareTo("KiloMeter")==0)
		{
			switch(convert_to)
			{
			case "Inch" : converted_value = converted_value * 39370.1; break;
			case "Feet" : converted_value = converted_value * 3280.84; break;
			case "Meter" : converted_value = converted_value * 1000; break;
			case "Mile": converted_value = converted_value * 0.621371; break;
			}
		}
		else if(convert_from.compareTo("CentiMeter")==0)
		{
			switch(convert_to)
			{
			case "MicroMeter" : converted_value = converted_value * 10000; break;
			case "MiliMeter" : converted_value = converted_value * 10;break;
			case "Inch" : converted_value = converted_value * 0.393701;break;
			case "DeciMeter" : converted_value = converted_value * 0.1;break;
			case "Feet" : converted_value = converted_value * 0.0328084;break;
			case "Meter" : converted_value = converted_value * 0.01;
			}
			
		}
		else if(convert_from.compareTo("Mile")==0)
		{
			switch(convert_to)
			{
			case "Feet" : converted_value = converted_value * 5280; break;
			case "Meter" : converted_value = converted_value * 1609.34; break;
			case "KiloMeter": converted_value = converted_value * 1.60934; break;
			}
		}
		else if(convert_from.compareTo("Gram")==0) {
			switch(convert_to)
			{
			case "KiloGram" : converted_value = converted_value * 0.001;break;
			case "Pound" : converted_value = converted_value * 0.00220462;break;
			case "Ton" : converted_value = converted_value/1000000.0;break;
			}
		}
		else if(convert_from.compareTo("KiloGram")==0) {
			switch(convert_to)
			{
			case "Gram" : converted_value = converted_value * 1000.0;break;
			case "Pound" : converted_value = converted_value * 2.20462;break;
			case "Ton" : converted_value = converted_value*0.001;break;
			}
		}
		else if(convert_from.compareTo("Pound")==0) {
			switch(convert_to)
			{
			case "Gram" : converted_value = converted_value * 453.592;break;
			case "KiloGram" : converted_value = converted_value * 0.453592;break;
			case "Ton" : converted_value = converted_value*0.0005;break;
			}
		}
		else if(convert_from.compareTo("Celsius")==0)
		{
			switch(convert_to)
			{
			case "Fahrenheit" : converted_value = (converted_value * 1.8) + 32;break;
			case "Kelvin" : converted_value = converted_value + 273.15 ;break;
			}
		}
		else if(convert_from.compareTo("Fahrenheit")==0)
		{
			switch(convert_to)
			{
			case "Celsius" : converted_value = 32 * converted_value - 32 * 5/9.0;break;
			case "Kelvin" : converted_value = converted_value + 273.15 ;break;
			}
		}
		return converted_value;
	}
}
