package application;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.net.URL;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
public class Conversions_Controller implements Initializable {
	Conversions obj = new Conversions();
	private String [] gramscales = {"KiloGram","Pound","Ton"};
	private String [] kilogramscales = {"Gram","Pound","Ton"};
	private String [] poundscales = {"Gram","KiloGram","Ton"};
	private String [] Celsiusscales = {"Fahrenheit","Kelvin"};
	private String [] Fahrenheitscales = {"Celsius","Kelvin"};
 	private String [] Kilometerscales = {"Inch","Feet","Meter","Mile"};
	private String [] Milescales = {"Feet","Meter","KiloMeter"};
	private String [] centimeterscales = {"Inch","Feet","MicroMeter","MiliMeter","DeciMeter","Meter"};
	private String [] meterscales = {"MicroMeter","MiliMeter","CentiMeter","Inch","Feet","KiloMeter","Mile"};
	private String [] scales = {"Meter","KiloMeter","CentiMeter","Mile","Gram","KiloGram","Pound","Celsius","Fahrenheit"};
	@FXML
	private Button calculate_con;
	@FXML
	private Label st_scale,nd_scale;
	@FXML
	private TextField scale_filed1,scale_filed2;
	@FXML
	private ChoiceBox dropdown1,dropdown2;
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		scale_filed2.setEditable(false);
		dropdown1.getItems().addAll(scales);
		dropdown1.setOnAction((event) -> {
		    if(dropdown1.getValue()=="Meter")
		    {
		    	dropdown2.getItems().clear();
		    	dropdown2.getItems().setAll(meterscales);
		    }
		    else if(dropdown1.getValue()=="KiloMeter")
		    {
		    	dropdown2.getItems().clear();
		    	dropdown2.getItems().setAll(Kilometerscales);
		    }
		    else if(dropdown1.getValue()=="CentiMeter")
		    {
		    	dropdown2.getItems().clear();
		    	dropdown2.getItems().setAll(centimeterscales);
		    }
		    else if(dropdown1.getValue()=="Mile")
		    {
		    	dropdown2.getItems().clear();
		    	dropdown2.getItems().setAll(Milescales);
		    }
		    else if(dropdown1.getValue()=="Gram")
		    {
		    	dropdown2.getItems().clear();
		    	dropdown2.getItems().setAll(gramscales);
		    }
		    else if(dropdown1.getValue()=="KiloGram")
		    {
		    	dropdown2.getItems().clear();
		    	dropdown2.getItems().setAll(kilogramscales);
		    }
		    else if(dropdown1.getValue()=="Pound")
		    {
		    	dropdown2.getItems().clear();
		    	dropdown2.getItems().setAll(poundscales);
		    }
		    else if(dropdown1.getValue()=="Celsius")
		    {
		    	dropdown2.getItems().clear();
		    	dropdown2.getItems().setAll(Celsiusscales);
		    }
		    else if(dropdown1.getValue()=="Fahrenheit")
		    {
		    	dropdown2.getItems().clear();
		    	dropdown2.getItems().setAll(Fahrenheitscales);
		    }
		});
	}
	@FXML
	protected void convert_act(ActionEvent event) throws IOException
	{
		scale_filed2.setText(Double.toString(obj.convertlength(dropdown1.getValue().toString(),dropdown2.getValue().toString(),Double.parseDouble(scale_filed1.getText()))));
	}

}