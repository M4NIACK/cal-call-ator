package application;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.net.URL;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;

public class Main_Menu_Controller { 
	@FXML
	private Label Main_Menu_label;
	@FXML
	private Button GPA_button,BMR_button,Calculator_button,Currency_button,Conversions_button;
	@FXML
	protected void onGPAbuttonclicked(ActionEvent event) throws IOException
	{
		AnchorPane root1 = (AnchorPane)FXMLLoader.load(getClass().getResource("GPA_Calculator.fxml"));
		Scene scene1 = new Scene(root1,400,400);
		Stage primaryStage = new Stage();
		primaryStage.setScene(scene1);
		primaryStage.setResizable(false);
		primaryStage.setTitle("GPA Calculator");
		primaryStage.show();
	}
	@FXML
	protected void onBMRbuttonclicked(ActionEvent event) throws IOException
	{
		AnchorPane root2 = (AnchorPane)FXMLLoader.load(getClass().getResource("BMR_Calculator.fxml"));
		Scene scene2 = new Scene(root2,400,400);
		Stage primaryStage = new Stage();
		primaryStage.setScene(scene2);
		primaryStage.setResizable(false);
		primaryStage.setTitle("BMR Calculator");
		primaryStage.show();
	}
	@FXML
	protected void onConversionsbuttonclicked(ActionEvent event) throws IOException
	{
		AnchorPane root3 = (AnchorPane)FXMLLoader.load(getClass().getResource("Conversions.fxml"));
		Scene scene3 = new Scene(root3,400,400);
		Stage primaryStage = new Stage();
		primaryStage.setScene(scene3);
		primaryStage.setResizable(false);
		primaryStage.setTitle("Conversions");
		primaryStage.show();
	}
	@FXML
	protected void oncurrencysbuttonclicked(ActionEvent event) throws IOException
	{
		AnchorPane root4 = (AnchorPane)FXMLLoader.load(getClass().getResource("Currency.fxml"));
		Scene scene4 = new Scene(root4,400,400);
		Stage primaryStage = new Stage();
		primaryStage.setScene(scene4);
		primaryStage.setResizable(false);
		primaryStage.setTitle("Currency Converter");
		primaryStage.show();
	}
	@FXML
	protected void oncalc(ActionEvent event) throws IOException
	{
		AnchorPane root4 = (AnchorPane)FXMLLoader.load(getClass().getResource("Calculaotr.fxml"));
		Scene scene4 = new Scene(root4,500,500);
		Stage primaryStage = new Stage();
		primaryStage.setScene(scene4);
		primaryStage.setResizable(false);
		primaryStage.setTitle("Calculator");
		primaryStage.show();
	}



	
}
