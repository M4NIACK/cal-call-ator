package application;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.net.URL;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
public class Calculator_Controller implements Initializable {
	Calculator obj = new Calculator();
	@FXML
	private Button btn_1,btn_2,btn_3,btn_4,btn_5,btn_6,btn_0,btn_7,btn_8,btn_9,btn_plus,btn_sub,btn_mul,btn_div,btn_eq,btn_dot,re,clear;
	@FXML
	private TextField fieldof;
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		fieldof.setEditable(false);
		btn_eq.setDisable(true);
	}
	@FXML
	protected void func1(ActionEvent event) throws IOException
	{
		fieldof.setText(fieldof.getText()+"1");
	}
	@FXML
	protected void func2(ActionEvent event) throws IOException
	{
		fieldof.setText(fieldof.getText()+"2");
	}
	@FXML
	protected void func3(ActionEvent event) throws IOException
	{
		fieldof.setText(fieldof.getText()+"3");
	}
	@FXML
	protected void func4(ActionEvent event) throws IOException
	{
		fieldof.setText(fieldof.getText()+"4");
	}
	@FXML
	protected void func5(ActionEvent event) throws IOException
	{
		fieldof.setText(fieldof.getText()+"5");
	}
	@FXML
	protected void func0(ActionEvent event) throws IOException
	{
		fieldof.setText(fieldof.getText()+"0");
	}
	@FXML
	protected void func6(ActionEvent event) throws IOException
	{
		fieldof.setText(fieldof.getText()+"6");
	}
	@FXML
	protected void func7(ActionEvent event) throws IOException
	{
		fieldof.setText(fieldof.getText()+"7");
	}
	@FXML
	protected void func8(ActionEvent event) throws IOException
	{
		fieldof.setText(fieldof.getText()+"8");
	}
	@FXML
	protected void func9(ActionEvent event) throws IOException
	{
		fieldof.setText(fieldof.getText()+"9");
	}
	@FXML
	protected void funcdot(ActionEvent event) throws IOException
	{
		if(fieldof.getText().contains(".")==true)
		{
		}
		else
		{
			fieldof.setText(fieldof.getText()+".");
		}
	}
	@FXML
	protected void funcadd(ActionEvent event) throws IOException
	{
		btn_eq.setDisable(false);
		obj.setSign("+");
		obj.setFirst_num(Double.parseDouble(fieldof.getText()));
		obj.setSecond_num(0);
		fieldof.clear();

	}
	@FXML
	protected void funcsub(ActionEvent event) throws IOException
	{
		btn_eq.setDisable(false);
		obj.setSign("-");
		obj.setFirst_num(Double.parseDouble(fieldof.getText()));
		obj.setSecond_num(0);
		fieldof.clear();

	}
	@FXML
	protected void funcmul(ActionEvent event) throws IOException
	{
		btn_eq.setDisable(false);
		obj.setSign("*");
		obj.setFirst_num(Double.parseDouble(fieldof.getText()));
		obj.setSecond_num(0);
		fieldof.clear();

	}
	@FXML
	protected void funcdiv(ActionEvent event) throws IOException
	{
		btn_eq.setDisable(false);
		obj.setSign("/");
		obj.setFirst_num(Double.parseDouble(fieldof.getText()));
		obj.setSecond_num(0);
		fieldof.clear();
	}
	@FXML
	protected void funceq(ActionEvent event) throws IOException
	{
		if(fieldof.getText().isEmpty())
		{
		fieldof.setText(Double.toString(obj.calculate()));
		obj.setFirst_num(Double.parseDouble(fieldof.getText()));
		obj.setSecond_num(0);
		}
		else
		{
			obj.setSecond_num(Double.parseDouble(fieldof.getText()));
			fieldof.setText(Double.toString(obj.calculate()));
			obj.setFirst_num(Double.parseDouble(fieldof.getText()));
			obj.setSecond_num(0);
		}
	}
	@FXML
	protected void ref(ActionEvent event) throws IOException
	{
		fieldof.setText(fieldof.getText().substring(0, fieldof.getText().length()-1));
	}
	@FXML
	protected void clearf(ActionEvent event) throws IOException
	{
		fieldof.clear();
		obj.setFirst_num(0);
		obj.setSecond_num(0);
	}
	
}