package application;
public class Calculator
{
	private double first_num,second_num;
	public double getFirst_num() {
		return first_num;
	}
	public void setFirst_num(double first_num) {
		this.first_num = first_num;
	}
	public double getSecond_num() {
		return second_num;
	}
	public void setSecond_num(double second_num) {
		this.second_num = second_num;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	private String sign;
	public double calculate()
	{
		if(sign.compareTo("+")==0)
		{
			return this.first_num + this.second_num;
		}
		else if(sign.compareTo("*")==0)
		{
			return this.first_num * this.second_num;
		}
		else if(sign.compareTo("/")==0)
		{
			return this.first_num / this.second_num;
		}
		else if(sign.compareTo("-")==0)
		{
			return this.first_num - this.second_num;
		}
		return 0;
	}
}