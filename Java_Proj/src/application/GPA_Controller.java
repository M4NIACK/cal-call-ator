package application;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.net.URL;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
public class GPA_Controller implements Initializable {
	public GPA_Calculator obj = new GPA_Calculator();
	@FXML
	private Label GPA_label,course_name,course_credits,course_grade,result_gpa;
	@FXML
	private Button add_course,GPA_calculate,clear_courses;
	@FXML
	private TextField course_name_text,course_credits_text,Result_text;
	@FXML
	private ChoiceBox <String> dropmenu;
	private String [] Grades = {"A","A-","B+","B","B-","C+","C","C-","D+","D","F"};
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		dropmenu.getItems().addAll(Grades);
		Result_text.setEditable(false);
	}
	@FXML
	protected void addcourse_btn(ActionEvent event) throws IOException
	{
		obj.addcourse(course_name_text.getText(), dropmenu.getValue(),Integer.parseInt(course_credits_text.getText()));
		course_name_text.clear();
		course_credits_text.clear();
	}
	@FXML
	protected void calculatecourse_btn(ActionEvent event) throws IOException
	{
		Result_text.setText(Double.toString(obj.calculate_gpa()));
	}
	@FXML
	protected void clearcourse_btn(ActionEvent event) throws IOException
	{
		obj.clearlist();
	}
	
}
