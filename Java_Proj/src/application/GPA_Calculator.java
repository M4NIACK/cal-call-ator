package application;

import java.util.ArrayList;

public class GPA_Calculator {
	private ArrayList <courses> courselist = new ArrayList <courses>();
	public void clearlist()
	{
		courselist.clear();
	}
	public boolean addcourse(String course_name,String course_grade, int course_credits)
	{
		if(course_credits>=0)
		{
		courses new_course = new courses();
		new_course.setCourse_name(course_name);
		new_course.setCourse_grade(course_grade);
		new_course.setCourse_int(course_credits);
		courselist.add(new_course);
		return true;
		}
		return false;
	}
	public double calculate_gpa()
	{
		double x = 0;
		int y = 0;
		for(int i=0;i<courselist.size();i++)
		{
			y=y+courselist.get(i).getCourse_int();
			switch(courselist.get(i).getCourse_grade())
			{
			case "A" : x = x + (4*courselist.get(i).getCourse_int());
			break;
			case "A-": x = x + (3.7 * courselist.get(i).getCourse_int());
			break;
			case "B+": x = x + (3.3 * courselist.get(i).getCourse_int());
			break;
			case "B": x = x + (3 * courselist.get(i).getCourse_int());
			break;
			case "B-": x = x + (2.7 * courselist.get(i).getCourse_int());
			break;
			case "C+": x = x + (2.3 * courselist.get(i).getCourse_int());
			break;
			case "C": x = x + (2 * courselist.get(i).getCourse_int());
			break;
			case "C-": x = x + (1.7 * courselist.get(i).getCourse_int());
			break;
			case "D+": x = x + (1.3 * courselist.get(i).getCourse_int());
			break;
			case "D": x = x + (1 * courselist.get(i).getCourse_int());
			break;
			case "F": x = x + (0 * courselist.get(i).getCourse_int());
			break;
				
			}
		}
		return x/y;
		
	}
}
