package application;


public class courses {
	private String course_name;
	private String course_grade;
	private int course_int;
	public String getCourse_name() {
		return course_name;
	}
	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}
	public String getCourse_grade() {
		return course_grade;
	}
	public void setCourse_grade(String course_grade) {
		this.course_grade = course_grade;
	}
	public int getCourse_int() {
		return course_int;
	}
	public void setCourse_int(int course_int) {
		this.course_int = course_int;
	}
}
