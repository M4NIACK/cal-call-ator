package application;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.net.URL;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
public class BMR_Controller implements Initializable {
	@FXML
	private Label height,weight,age,gender,result;
	@FXML
	private TextField Height_field,Weight_field,Age_field,Result_field;
	@FXML
	private Button BMR_Calculate;
	@FXML
	private RadioButton Male,Female;
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		Result_field.setEditable(false);
	}
	@FXML
	protected void CalcBMR(ActionEvent event) throws IOException
	{
		if(Male.isSelected()==true)
		{
		BMR_Calculator obj = new BMR_Calculator(Double.parseDouble(Height_field.getText()),Double.parseDouble(Weight_field.getText()),Integer.parseInt(Age_field.getText()),"Male");
		Result_field.setText(Double.toString(obj.BMR_value()));	
		}
		else
		{
		BMR_Calculator obj = new BMR_Calculator(Double.parseDouble(Height_field.getText()),Double.parseDouble(Weight_field.getText()),Integer.parseInt(Age_field.getText()),"Female");
		Result_field.setText(Double.toString(obj.BMR_value()));	
		}
	}
}
