import customtkinter

root = customtkinter.CTk()
root.geometry("300x400")
gpa_gui = customtkinter.CTkFrame(root)
gpa_gui.pack(pady=20, padx=50, expand=True, fill="both")

def cred_callback(selection):
    cred = selection
    print(cred)

total = 0

def calc_gpa(credit, grade):

    cred = int(credit)
    
    if grade == 'A+/A':
        gpa_total = cred * 4
    elif grade == 'A-':
        gpa_total = cred * 3.7
    elif grade == 'B+':
        gpa_total = cred * 3.3
    elif grade == 'B':
        gpa_total = cred * 3
    elif grade == 'B-':
        gpa_total = cred * 2.7
    elif grade == 'C+':
        gpa_total = cred * 2.3
    elif grade == 'C':
        gpa_total = cred * 2
    elif grade == 'C-':
        gpa_total = cred * 1.7
    elif grade == 'D+':
        gpa_total = cred * 1.2
    elif grade == 'D':
        gpa_total = cred * 1
    elif grade == 'F':
        gpa_total = cred * 0
    course_name.delete(0, course_name.index(customtkinter.END))
    print(gpa_total)
    global total
    total = total + gpa_total
    total_gpa.configure(text=f'Total GPA: {total}')


label = customtkinter.CTkLabel(gpa_gui, text="GPA Calculator", font=("Arial", 16, "bold"))
label.pack()

course_name = customtkinter.CTkEntry(gpa_gui, placeholder_text="Course Name", justify="center")
course_name.pack(pady=10)

cred_menu = customtkinter.CTkOptionMenu(gpa_gui, values=['4', '3', '2', '1'], button_hover_color="green", command=cred_callback)
cred_menu.pack(pady=10, padx=5)
cred_menu.set("Credit Hour")

grade_menu = customtkinter.CTkOptionMenu(gpa_gui, values=['A+/A', 'A-', 'B+', 'B', 'B-', 'C+', 'C', 'C-', 'D+', 'D', 'F'], button_hover_color="green", command=cred_callback)
grade_menu.pack(pady=10)
grade_menu.set("Grade")

total_gpa = customtkinter.CTkLabel(gpa_gui, text="", font=("Arial", 14, "bold"))
total_gpa.pack()

gpa_calc = customtkinter.CTkButton(gpa_gui,text="Calculate", command=lambda:calc_gpa(cred_menu.get(), grade_menu.get()))
gpa_calc.pack()

root.mainloop()