import customtkinter

win = customtkinter.CTk()
win.geometry('300x300')
calc_gui = customtkinter.CTkFrame(win)

calc_gui.pack(fill='both', expand=True, pady=10, padx=10)
# calc_gui.configure(width=600, height=200)
calculation = ""

def add_to_calculation(symbol):
    global calculation
    calculation = calculation + str(symbol)
    result_field.delete(1.0, result_field.index(customtkinter.END))
    result_field.insert(1.0, calculation)

def evaluate():
    global calculation
    try:
        calculation = str(eval(calculation))
        result_field.delete(1.0, result_field.index(customtkinter.END))
        result_field.insert(1.0, calculation)
    except:
        clear()
        result_field.insert(1.0, "ERROR")

def clear():
    global calculation
    calculation = ""
    result_field.delete(1.0, result_field.index(customtkinter.END))
    
result_field = customtkinter.CTkTextbox(calc_gui, height=15, width=275, font=("Arial", 24))
result_field.grid(columnspan=5, pady=10)

btn1 = customtkinter.CTkButton(calc_gui, text="1", command=lambda: add_to_calculation(1), width=40, height=40)
btn1.grid(row=2, column=1)

btn2 = customtkinter.CTkButton(calc_gui, text="2", command=lambda: add_to_calculation(2), width=40,height =40)
btn2.grid(row=2, column=2)

btn3 = customtkinter.CTkButton(calc_gui, text="3", command=lambda: add_to_calculation(3), width=40,height =40)
btn3.grid(row=2, column=3)

btn4 = customtkinter.CTkButton(calc_gui, text="4", command=lambda: add_to_calculation(4), width=40,height =40)
btn4.grid(row=3, column=1)

btn5 = customtkinter.CTkButton(calc_gui, text="5", command=lambda: add_to_calculation(5), width=40,height =40)
btn5.grid(row=3, column=2)

btn6 = customtkinter.CTkButton(calc_gui, text="6", command=lambda: add_to_calculation(6), width=40,height =40)
btn6.grid(row=3, column=3)

btn7 = customtkinter.CTkButton(calc_gui, text="7", command=lambda: add_to_calculation(7), width=40,height =40)
btn7.grid(row=4, column=1)

btn8 = customtkinter.CTkButton(calc_gui, text="8", command=lambda: add_to_calculation(8), width = 40, height =40)
btn8.grid(row=4, column=2)

btn9 = customtkinter.CTkButton(calc_gui, text="9", command=lambda: add_to_calculation(9), width = 40,height =40)
btn9.grid(row=4, column=3)

btn0 = customtkinter.CTkButton(calc_gui, text="0", command=lambda: add_to_calculation(0), width=40,height =40)
btn0.grid(row=5, column=2)

plus = customtkinter.CTkButton(calc_gui, text="+", command=lambda: add_to_calculation("+"), width=40, height=40)
plus.grid(row=2, column=4)

minus = customtkinter.CTkButton(calc_gui, text="-", command=lambda: add_to_calculation("-"), width=40, height=40)
minus.grid(row=3, column=4)

mul = customtkinter.CTkButton(calc_gui, text="x", command=lambda: add_to_calculation("*"), width=40, height=40)
mul.grid(row=4, column=4)

div = customtkinter.CTkButton(calc_gui, text="/", command=lambda: add_to_calculation("/"), width=40, height=40)
div.grid(row=5, column=4)

open = customtkinter.CTkButton(calc_gui, text="(", command=lambda: add_to_calculation("("), width=40, height=40)
open.grid(row=5, column=1)

close = customtkinter.CTkButton(calc_gui, text=")", command=lambda: add_to_calculation(")"), width=40, height=40)
close.grid(row=5, column=3)

equal = customtkinter.CTkButton(calc_gui, text="=", command=evaluate, width=110, height=40)
equal.grid(row=6, column=3, columnspan=2)

clear = customtkinter.CTkButton(calc_gui, text="C", command= clear, width=110, height=40)
clear.grid(row=6, column=1, columnspan=2)
win.mainloop()