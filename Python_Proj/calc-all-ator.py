import tkinter as tk
import customtkinter
from forex_python.converter import CurrencyRates

customtkinter.set_appearance_mode("system")
customtkinter.set_default_color_theme("dark-blue")

root = customtkinter.CTk()
root.geometry("300x420")
root.title("Calc-All-ator")

def main():
########################################################################
# Currency Conversion
######################################################################## 
    def currency_conv():
        def back():
            currency.destroy()
            main()

        frame.destroy()
        cr = CurrencyRates()
        currency = customtkinter.CTkFrame(root)
        currency.pack(pady=20, padx=60, fill="both", expand=True)

        def conv(from_cr, to_cr):
            money = float(amount.get())
            output = cr.convert(from_cr, to_cr, money)
            output = round(output, 2)
            conv_label.configure(text=f'Converting from {from_cr} to {to_cr}:')
            result.delete(0, result.index(tk.END)-1)
            result.insert(0, output)

        def from_callback(selection):
            from_value = selection

        def to_callback(selection):
            to_value = selection

        root.title("Currency Converter")

        label = customtkinter.CTkLabel(currency, text="Currency Converter", font=("Arial",16,"bold"))
        label.pack(pady=10, padx=10)

        amount = customtkinter.CTkEntry(currency, placeholder_text="Amount", justify="center")
        amount.pack(pady=7, padx=10)

        label_from = customtkinter.CTkLabel(currency, text = "Convert From: ")
        label_from.pack()

        option_from = customtkinter.CTkOptionMenu(currency, button_hover_color="green",values=["EUR", "USD", "GBP", "IDR", "BGN", "ILS", "DKK", "CAD", "JPY", "HUF", "RON", "MYR", "SEK", "SGD", "HKD", "AUD", "CHF", "KRW", "CNY", "TRY", "HRK", "NZD", "THB", "NOK", "RUB", "MXN", "CZK", "BRL", "PLN", "PHP", "ZAR"], command=from_callback)
        option_from.pack(pady=6, padx=10)
        option_from.set("Convert From")

        label_to = customtkinter.CTkLabel(currency, text="Convert To: ").pack()
        option_to = customtkinter.CTkOptionMenu(currency, button_hover_color="green", values=["EUR", "USD", "GBP", "IDR", "BGN", "ILS", "DKK", "CAD", "JPY", "HUF", "RON", "MYR", "SEK", "SGD", "HKD", "AUD", "CHF", "KRW", "CNY", "TRY", "HRK", "NZD", "THB", "NOK", "RUB", "MXN", "CZK", "BRL", "PLN", "PHP", "ZAR"], command=to_callback)
        option_to.pack(pady=6, padx=10)
        option_to.set("Convert To")

        back_button = customtkinter.CTkButton(currency, text="Back", command=lambda: back())
        back_button.pack(side='bottom', pady=5)

        button = customtkinter.CTkButton(currency, text="Convert", command= lambda:conv(option_from.get(), option_to.get()))
        button.pack(side="bottom")

        conv_label = customtkinter.CTkLabel(currency, text="", font=("Arial", 12))
        conv_label.pack()

        result = customtkinter.CTkEntry(currency, placeholder_text="Converting...", justify="center")
        result.pack()
########################################################################
# BMR Calculator
########################################################################
    def bmr_calc():
        def back():
            bmr_gui.destroy()
            main()
        frame.destroy()
        root.title("BMR Calculator")
        bmr_gui = customtkinter.CTkFrame(master=root)
        bmr_gui.pack(pady=20, padx=50, fill="both", expand=True)
        
        def gen_callback(selection):
            gender = selection
            print(gender)

        def calculate(gender):
            w = float(weight.get())
            h = float(height.get())
            a = float(age.get())

            if(gender.lower() == 'male'):
                bmr = 66.5 + (13.75 * w) + (5.003 * h) - (6.75 * a)
            elif(gender.lower() == 'female'):
                bmr = 655.1 + (9.563 * w) + (1.850 * h) - (4.676 * a)
            BMR.delete(0, BMR.index(tk.END)-1)
            BMR.insert(0, bmr)

        label = customtkinter.CTkLabel(bmr_gui, text="BMR Calculator", font=("Arial",16,"bold"))
        label.pack(pady=10, padx=5)

        height = customtkinter.CTkEntry(bmr_gui, placeholder_text="Enter your height (cm)", justify="center")
        height.pack(pady=10, padx=5)

        weight = customtkinter.CTkEntry(bmr_gui, placeholder_text="Enter your weight (kg)", justify="center")
        weight.pack(pady=10, padx=5)

        age = customtkinter.CTkEntry(bmr_gui, placeholder_text="Enter your age", justify="center")
        age.pack(pady=10, padx=5)

        gender_menu = customtkinter.CTkOptionMenu(bmr_gui, values=["Male", "Female"], button_hover_color="green", command=gen_callback)
        gender_menu.pack(pady=10, padx=5)
        gender_menu.set("Gender")

        back_button = customtkinter.CTkButton(bmr_gui, text="Back", command=lambda: back())
        back_button.pack(side='bottom', pady=5)

        button = customtkinter.CTkButton(bmr_gui, text="Calculate", command=lambda:calculate(gender_menu.get()))
        button.pack(pady=5, side="bottom")

        BMR = customtkinter.CTkEntry(bmr_gui, placeholder_text="Calculating...", justify="center")
        BMR.pack(pady=10)
        frame.destroy()
########################################################################
# Length Converter
########################################################################
    def length_func():
        frame.destroy()
        
        def back():
            len_gui.destroy()
            main()

        def conv(from_len, to_len):
            result.delete(0, result.index(customtkinter.END)-1)
            length = float(Length.get())
            conv_label.configure(text=f'Converting from {from_len} to {to_len}:')
            if from_len == to_len:
                result.insert(0, Length.get())
            
        # From meters
            elif from_len == 'Meter':
                if to_len == 'Kilometers':
                    length = length / 1000
                
                elif to_len == 'Centimeter':
                    length = length * 100
                
                elif to_len == 'Millimeter':
                    length = length * 1000

                elif to_len == 'Mile':
                    length = length / 1609.34
                
                elif to_len=='Inch':
                    length = length * 39.3701
                
                elif to_len=='Foot':
                    length = length * 3.28084   
        # From inches
            elif from_len == 'Inch':
                if to_len == 'Meter':
                    length = length / 39.3701
                
                elif to_len == 'Foot':
                    length = length / 12
                
                elif to_len == 'Kilometers':
                    length = length / 39370.1

                elif to_len == 'Centimeter':
                    length = length / 0.393701

                elif to_len == 'Millimeter':
                    length = length * 25.4
                
                elif to_len == 'Mile':
                    length = length / 63360
        # From Foot
            elif from_len == 'Foot':
                if to_len == 'Meter':
                    length = length / 3.28084

                elif to_len == 'Inch':
                    length = length * 12
                
                elif to_len == 'Kilometers':
                    length = length / 3280.84
                
                elif to_len == 'Centimeter':
                    length = length * 30.48

                elif to_len == 'Millimeter':
                    length = length * 304.8

                elif to_len == 'Mile':
                    length = length / 5280

        # From Kilometers
            elif from_len == 'Kilometers':
                if to_len == 'Meter':
                    length = length * 1000
                
                elif to_len == 'Centimeter':
                    length = length * 100000
                
                elif to_len == 'Millimeter':
                    length = length * 10000000

                elif to_len == 'Mile':
                    length = length * 0.621371
                
                elif to_len == 'Inch':
                    length = length * 39370.1
                
                elif to_len == 'Foot':
                    length = length * 3280.84
        # From Centimeters
            elif from_len == 'Centimeter':
                if to_len == 'Kilometers':
                    length = length / 100000
                
                elif to_len == 'Meter':
                    length = length / 100
                
                elif to_len == 'Millimeter':
                    length = length * 10
                
                elif to_len == 'Mile':
                    length = length / 160934
            
                elif to_len == 'Inch':
                    length = length / 2.54
                
                elif to_len == 'Foot':
                    length = length / 30.48

        #  From Millimeters
            elif from_len == 'Millimeter':
                if to_len == 'Meter':
                    length = length / 1000
                
                elif to_len == 'Kilometers':
                    length = length / 1000000
                
                elif to_len == 'Centimeter':
                    length = length / 10
                
                elif to_len == 'Mile':
                    length = length * 1609344

                elif to_len == 'Inch':
                    length = length * 25.4

                elif to_len == 'Foot':
                    length = length * 304.8
            
        # From Mile
            elif from_len == 'Mile':
                
                if to_len == 'Meter':
                    length = length * 1609.34
                
                elif to_len == 'Kilometers':
                    length = length * 1.60934
                
                elif to_len == 'Centimeter':
                    length = length * 160934
                
                elif to_len == 'Millimeter':
                    length = length * 1609340

                elif to_len == 'Inch':
                    length = length * 63360
                
                elif to_len == 'Foot':
                    length = length * 5280
                

                
            length = round(length,2)
            result.insert(0, str(length))

        def from_callback(selection):
            from_value = selection

        def to_callback(selection):
            to_value = selection

        root.title("Length Units Converter")
        len_gui = customtkinter.CTkFrame(master=root)
        len_gui.pack(pady=20, padx=60, fill="both", expand=True)

        label = customtkinter.CTkLabel(master=len_gui, text="Length Converter", font=("Arial",16,"bold"))
        label.pack(pady=10, padx=10)

        Length = customtkinter.CTkEntry(master=len_gui, placeholder_text="Length", justify="center")
        Length.pack(pady=7, padx=10)

        label_from = customtkinter.CTkLabel(len_gui, text = "Convert From: ")
        label_from.pack()

        option_from = customtkinter.CTkOptionMenu(len_gui, button_hover_color="green",values=['Meter','Kilometers', 'Centimeter', 'Millimeter', 'Mile', 'Inch', 'Foot'], command=from_callback)
        option_from.pack(pady=6, padx=10)
        option_from.set("Convert From")

        label_to = customtkinter.CTkLabel(len_gui, text="Convert To: ").pack()

        option_to = customtkinter.CTkOptionMenu(len_gui, button_hover_color="green", values=['Meter','Kilometers', 'Centimeter', 'Millimeter', 'Mile', 'Inch', 'Foot'], command=to_callback)
        option_to.pack(pady=6, padx=10)
        option_to.set("Convert To")

        back_button = customtkinter.CTkButton(len_gui, text="Back", command=back)
        back_button.pack(side='bottom',pady=10)
        
        button = customtkinter.CTkButton(len_gui, text="Convert", command= lambda:conv(option_from.get(), option_to.get()))
        button.pack(side="bottom")

        conv_label = customtkinter.CTkLabel(len_gui, text="", font=("Arial", 12))
        conv_label.pack()

        result = customtkinter.CTkEntry(len_gui, placeholder_text="Converting...", justify="center")
        result.pack()
########################################################################
# Weight Converter
########################################################################
    def weight_func():
        frame.destroy()
        
        def back():
            weight_gui.destroy()
            main()

        def conv(from_weight, to_weight):
            result.delete(0, result.index(customtkinter.END)-1)
            conv_label.configure(text=f'Converting from {from_weight} to {to_weight}:')
            weight = float(Weight.get())
        # From Kilogram
            if from_weight == 'Kilogram':
                if to_weight == 'Gram':
                    weight = weight * 1000
                elif to_weight == 'Milligram':
                    weight = weight * 1000000
                elif to_weight == 'Pound':
                    weight = weight * 2.20462
                elif to_weight == 'Ounce':
                    weight = weight * 35.274
            
            elif from_weight == 'Gram':
                if to_weight == 'Kilogram':
                    weight = weight / 1000
                elif to_weight == 'Milligram':
                    weight = weight * 1000
                elif to_weight == 'Pound':
                    weight = weight / 453.592
                elif to_weight == 'Ounce':
                    weight = weight / 28.3495

            elif from_weight == 'Milligram':
                if to_weight == 'Kilogram':
                    weight = weight / 1000000
                elif to_weight == 'Gram':
                    weight = weight / 1000
                elif to_weight == 'Pound':
                    weight = weight / 453592
                elif to_weight == 'Ounce':
                    weight = weight / 28349.5
            
            elif from_weight == 'Pound':
                if to_weight == 'Kilogram':
                    weight = weight / 2.20462
                elif to_weight == 'Gram':
                    weight = weight * 453.592
                elif to_weight == 'Milligram':
                    weight = weight * 453592
                elif to_weight == 'Ounce':
                    weight = weight * 16
            
            elif from_weight == 'Ounce':
                if to_weight == 'Kilogram':
                    weight = weight / 35.274
                elif to_weight == 'Gram':
                    weight = weight * 28.3495
                elif to_weight == 'Milligram':
                    weight = weight * 28349.5
                elif to_weight == 'Pound':
                    weight = weight / 16
                
            result.insert(0, weight)

        def from_callback(selection):
            from_value = selection

        def to_callback(selection):
            to_value = selection

        root.title("Weight Units Converter")
        weight_gui = customtkinter.CTkFrame(master=root)
        weight_gui.pack(pady=20, padx=50, fill="both", expand=True)

        label = customtkinter.CTkLabel(master=weight_gui, text="Weight Converter", font=("Arial",16,"bold"))
        label.pack(pady=10, padx=10)

        Weight = customtkinter.CTkEntry(master=weight_gui, placeholder_text="Weight", justify="center")
        Weight.pack(pady=7, padx=10)

        label_from = customtkinter.CTkLabel(weight_gui, text = "Convert From: ")
        label_from.pack()

        option_from = customtkinter.CTkOptionMenu(weight_gui, button_hover_color="green",values=['Kilogram', 'Gram', 'Milligram', 'Pound', 'Ounce'], command=from_callback)
        option_from.pack(pady=6, padx=10)
        option_from.set("Convert From")

        label_to = customtkinter.CTkLabel(weight_gui, text="Convert To: ").pack()

        option_to = customtkinter.CTkOptionMenu(weight_gui, button_hover_color="green", values=['Kilogram', 'Gram', 'Milligram', 'Pound', 'Ounce'], command=to_callback)
        option_to.pack(pady=6, padx=10)
        option_to.set("Convert To")

        back_button = customtkinter.CTkButton(weight_gui, text="Back", command=back)
        back_button.pack(side="bottom", pady=5)

        button = customtkinter.CTkButton(weight_gui, text="Convert", command= lambda:conv(option_from.get(), option_to.get()))
        button.pack(side="bottom")

        conv_label = customtkinter.CTkLabel(weight_gui, text="", font=("Arial", 12))
        conv_label.pack()

        result = customtkinter.CTkEntry(weight_gui, placeholder_text="Converting...", justify="center")
        result.pack()

    def calc_func():
        root.geometry("300x350")
        calculation = ""
        def back():
            calc_gui.destroy()
            main()
        
        frame.destroy()
        calc_gui = customtkinter.CTkFrame(root)

        calc_gui.pack(fill='both', expand=True, pady=10, padx=10)
        # calc_gui.configure(width=600, height=200)
    
        def add_to_calculation(symbol):
            global calculation
            calculation = calculation + str(symbol)
            result_field.delete(1.0, result_field.index(customtkinter.END))
            result_field.insert(1.0, calculation)

        def evaluate():
            global calculation
            try:
                calculation = str(eval(calculation))
                result_field.delete(1.0, result_field.index(customtkinter.END))
                result_field.insert(1.0, calculation)
            except:
                clear()
                result_field.insert(1.0, "ERROR")

        def clear():
            global calculation
            calculation = ""
            result_field.delete(1.0, result_field.index(customtkinter.END))
            
        result_field = customtkinter.CTkTextbox(calc_gui, height=15, width=275, font=("Arial", 24))
        result_field.grid(columnspan=5, pady=10)

        btn1 = customtkinter.CTkButton(calc_gui, text="1", command=lambda: add_to_calculation(1), width=40, height=40)
        btn1.grid(row=2, column=1)

        btn2 = customtkinter.CTkButton(calc_gui, text="2", command=lambda: add_to_calculation(2), width=40,height =40)
        btn2.grid(row=2, column=2)

        btn3 = customtkinter.CTkButton(calc_gui, text="3", command=lambda: add_to_calculation(3), width=40,height =40)
        btn3.grid(row=2, column=3)

        btn4 = customtkinter.CTkButton(calc_gui, text="4", command=lambda: add_to_calculation(4), width=40,height =40)
        btn4.grid(row=3, column=1)

        btn5 = customtkinter.CTkButton(calc_gui, text="5", command=lambda: add_to_calculation(5), width=40,height =40)
        btn5.grid(row=3, column=2)

        btn6 = customtkinter.CTkButton(calc_gui, text="6", command=lambda: add_to_calculation(6), width=40,height =40)
        btn6.grid(row=3, column=3)

        btn7 = customtkinter.CTkButton(calc_gui, text="7", command=lambda: add_to_calculation(7), width=40,height =40)
        btn7.grid(row=4, column=1)

        btn8 = customtkinter.CTkButton(calc_gui, text="8", command=lambda: add_to_calculation(8), width = 40, height =40)
        btn8.grid(row=4, column=2)

        btn9 = customtkinter.CTkButton(calc_gui, text="9", command=lambda: add_to_calculation(9), width = 40,height =40)
        btn9.grid(row=4, column=3)

        btn0 = customtkinter.CTkButton(calc_gui, text="0", command=lambda: add_to_calculation(0), width=40,height =40)
        btn0.grid(row=5, column=2)

        plus = customtkinter.CTkButton(calc_gui, text="+", command=lambda: add_to_calculation("+"), width=40, height=40)
        plus.grid(row=2, column=4)

        minus = customtkinter.CTkButton(calc_gui, text="-", command=lambda: add_to_calculation("-"), width=40, height=40)
        minus.grid(row=3, column=4)

        mul = customtkinter.CTkButton(calc_gui, text="x", command=lambda: add_to_calculation("*"), width=40, height=40)
        mul.grid(row=4, column=4)

        div = customtkinter.CTkButton(calc_gui, text="/", command=lambda: add_to_calculation("/"), width=40, height=40)
        div.grid(row=5, column=4)

        open = customtkinter.CTkButton(calc_gui, text="(", command=lambda: add_to_calculation("("), width=40, height=40)
        open.grid(row=5, column=1)

        close = customtkinter.CTkButton(calc_gui, text=")", command=lambda: add_to_calculation(")"), width=40, height=40)
        close.grid(row=5, column=3)

        equal = customtkinter.CTkButton(calc_gui, text="=", command=evaluate, width=110, height=40)
        equal.grid(row=6, column=3, columnspan=2)

        clear = customtkinter.CTkButton(calc_gui, text="C", command= clear, width=110, height=40)
        clear.grid(row=6, column=1, columnspan=2)

        back_button = customtkinter.CTkButton(calc_gui, text="Back", command=back, width=100, height=40)
        back_button.grid(row=7, column=2, columnspan=2)
######################################################
# GPA Calculator
######################################################

    def gpa_calculator():
        frame.destroy()
        root.geometry("300x300")
        gpa_gui = customtkinter.CTkFrame(root)
        gpa_gui.pack(pady=20, padx=50, expand=True, fill="both")
        
        total=0

        def back():
            gpa_gui.destroy()
            main()

        def cred_callback(selection):
            cred = selection
            print(cred)



        def calc_gpa(credit, grade):

            cred = int(credit)
    
            if grade == 'A+/A':
                gpa_total = cred * 4
            elif grade == 'A-':
                gpa_total = cred * 3.7
            elif grade == 'B+':
                gpa_total = cred * 3.3
            elif grade == 'B':
                gpa_total = cred * 3
            elif grade == 'B-':
                gpa_total = cred * 2.7
            elif grade == 'C+':
                gpa_total = cred * 2.3
            elif grade == 'C':
                gpa_total = cred * 2
            elif grade == 'C-':
                gpa_total = cred * 1.7
            elif grade == 'D+':
                gpa_total = cred * 1.2
            elif grade == 'D':
                gpa_total = cred * 1
            elif grade == 'F':
                gpa_total = cred * 0
            course_name.delete(0, course_name.index(customtkinter.END))
            print(gpa_total)   
            global total
            total = total + gpa_total
            total_gpa.configure(text=f'Total GPA: {total}')


        label = customtkinter.CTkLabel(gpa_gui, text="GPA Calculator", font=("Arial", 16, "bold"))
        label.pack()

        course_name = customtkinter.CTkEntry(gpa_gui, placeholder_text="Course Name", justify="center")
        course_name.pack(pady=10)

        cred_menu = customtkinter.CTkOptionMenu(gpa_gui, values=['4', '3', '2', '1'], button_hover_color="green", command=cred_callback)
        cred_menu.pack(pady=10, padx=5)
        cred_menu.set("Credit Hour")

        grade_menu = customtkinter.CTkOptionMenu(gpa_gui, values=['A+/A', 'A-', 'B+', 'B', 'B-', 'C+', 'C', 'C-', 'D+', 'D', 'F'], button_hover_color="green", command=cred_callback)
        grade_menu.pack(pady=10)
        grade_menu.set("Grade")

        total_gpa = customtkinter.CTkLabel(gpa_gui, text="", font=("Arial", 14, "bold"))
        total_gpa.pack()

        gpa_calc = customtkinter.CTkButton(gpa_gui,text="Calculate", command=lambda:calc_gpa(cred_menu.get(), grade_menu.get()))
        gpa_calc.pack()
        

        back_btn = customtkinter.CTkButton(gpa_gui,text="Back", command=back)
        back_btn.pack(side='bottom')


    root.geometry("300x460")

    frame = customtkinter.CTkFrame(root)
    frame.pack(pady=20, padx=60, fill="both", expand=True)

    mainlabel = customtkinter.CTkLabel(frame, text="Choose a Module", justify="center", font=("Arial", 20, "bold"))
    mainlabel.pack(pady=20)

    currency_button = customtkinter.CTkButton(frame, text="Currency Converter", command=currency_conv)
    currency_button.pack(pady=15)

    bmr_button = customtkinter.CTkButton(frame, text="BMR Calculator", command=bmr_calc)
    bmr_button.pack(pady=15)

    lenght_button = customtkinter.CTkButton(frame, text="Lenght Converter", command=length_func)
    lenght_button.pack(pady=15)

    weight_button = customtkinter.CTkButton(frame, text="Weight Converter", command=weight_func)
    weight_button.pack(pady=15)

    calc_button = customtkinter.CTkButton(frame, text="Calculator", command=calc_func)
    calc_button.pack(pady=15)

    gpa_button = customtkinter.CTkButton(frame, text="GPA Calculator", command=gpa_calculator)
    gpa_button.pack(pady=15)

main()
root.mainloop()
