import customtkinter

customtkinter.set_appearance_mode("system")
customtkinter.set_default_color_theme("dark-blue")
root = customtkinter.CTk()
root.geometry("400x420")

def conv(from_len, to_len):
    result.delete(0, result.index(customtkinter.END)-1)
    length = float(Length.get())
    conv_label.configure(text=f'Converting from {from_len} to {to_len}:')
    if from_len == to_len:
        result.insert(0, Length.get())
    
# From meters
    elif from_len == 'Meter':
        if to_len == 'Kilometers':
            length = length / 1000
        
        elif to_len == 'Centimeter':
            length = length * 100
        
        elif to_len == 'Millimeter':
            length = length * 1000

        elif to_len == 'Mile':
            length = length / 1609.34
        
        elif to_len=='Inch':
            length = length * 39.3701
        
        elif to_len=='Foot':
            length = length * 3.28084   
# From inches
    elif from_len == 'Inch':
        if to_len == 'Meter':
            length = length / 39.3701
        
        elif to_len == 'Foot':
            length = length / 12
        
        elif to_len == 'Kilometers':
            length = length / 39370.1

        elif to_len == 'Centimeter':
            length = length / 0.393701

        elif to_len == 'Millimeter':
            length = length * 25.4
        
        elif to_len == 'Mile':
            length = length / 63360
# From Foot
    elif from_len == 'Foot':
        if to_len == 'Meter':
            length = length / 3.28084

        elif to_len == 'Inch':
            length = length * 12
        
        elif to_len == 'Kilometers':
            length = length / 3280.84
        
        elif to_len == 'Centimeter':
            length = length * 30.48

        elif to_len == 'Millimeter':
            length = length * 304.8

        elif to_len == 'Mile':
            length = length / 5280

# From Kilometers
    elif from_len == 'Kilometers':
        if to_len == 'Meter':
            length = length * 1000
        
        elif to_len == 'Centimeter':
            length = length * 100000
        
        elif to_len == 'Millimeter':
            length = length * 10000000

        elif to_len == 'Mile':
            length = length * 0.621371
        
        elif to_len == 'Inch':
            length = length * 39370.1
        
        elif to_len == 'Foot':
            length = length * 3280.84
# From Centimeters
    elif from_len == 'Centimeter':
        if to_len == 'Kilometers':
            length = length / 100000
        
        elif to_len == 'Meter':
            length = length / 100
        
        elif to_len == 'Millimeter':
            length = length * 10
        
        elif to_len == 'Mile':
            length = length / 160934
    
        elif to_len == 'Inch':
            length = length / 2.54
        
        elif to_len == 'Foot':
            length = length / 30.48

#  From Millimeters
    elif from_len == 'Millimeter':
        if to_len == 'Meter':
            length = length / 1000
        
        elif to_len == 'Kilometers':
            length = length / 1000000
        
        elif to_len == 'Centimeter':
            length = length / 10
        
        elif to_len == 'Mile':
            length = length * 1609344

        elif to_len == 'Inch':
            length = length * 25.4

        elif to_len == 'Foot':
            length = length * 304.8
    
# From Mile
    elif from_len == 'Mile':
        
        if to_len == 'Meter':
            length = length * 1609.34
        
        elif to_len == 'Kilometers':
            length = length * 1.60934
        
        elif to_len == 'Centimeter':
            length = length * 160934
        
        elif to_len == 'Millimeter':
            length = length * 1609340

        elif to_len == 'Inch':
            length = length * 63360
        
        elif to_len == 'Foot':
            length = length * 5280
        

        
    length = round(length,2)
    result.insert(0, str(length))

def from_callback(selection):
    from_value = selection

def to_callback(selection):
    to_value = selection

root.title("Length Units Converter")
frame = customtkinter.CTkFrame(master=root)
frame.pack(pady=20, padx=60, fill="both", expand=True)

label = customtkinter.CTkLabel(master=frame, text="Length Converter")
label.pack(pady=10, padx=10)

Length = customtkinter.CTkEntry(master=frame, placeholder_text="Length", justify="center")
Length.pack(pady=7, padx=10)

label_from = customtkinter.CTkLabel(frame, text = "Convert From: ")
label_from.pack()

option_from = customtkinter.CTkOptionMenu(frame, button_hover_color="green",values=['Meter','Kilometers', 'Centimeter', 'Millimeter', 'Mile', 'Inch', 'Foot'], command=from_callback)
option_from.pack(pady=6, padx=10)
option_from.set("Convert From")

label_to = customtkinter.CTkLabel(frame, text="Convert To: ").pack()

option_to = customtkinter.CTkOptionMenu(frame, button_hover_color="green", values=['Meter','Kilometers', 'Centimeter', 'Millimeter', 'Mile', 'Inch', 'Foot'], command=to_callback)
option_to.pack(pady=6, padx=10)
option_to.set("Convert To")

button = customtkinter.CTkButton(frame, text="Convert", command= lambda:conv(option_from.get(), option_to.get()))
button.pack(side="bottom")

conv_label = customtkinter.CTkLabel(frame, text="", font=("Arial", 16))
conv_label.pack()

result = customtkinter.CTkEntry(frame, placeholder_text="Converting...", justify="center")
result.pack()

root.mainloop()