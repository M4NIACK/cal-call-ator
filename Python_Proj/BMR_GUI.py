import customtkinter

customtkinter.set_appearance_mode("system")
customtkinter.set_default_color_theme("dark-blue")

root = customtkinter.CTk()
root.geometry("370x400")
root.title("BMR Calculator")

def gen_callback(selection):
    gender = selection
    print(gender)

def calculate(gender):
    w = float(weight.get())
    h = float(height.get())
    a = float(age.get())

    if(gender.lower() == 'male'):
        bmr = 66.5 + (13.75 * w) + (5.003 * h) - (6.75 * a)
    elif(gender.lower() == 'female'):
        bmr = 655.1 + (9.563 * w) + (1.850 * h) - (4.676 * a)
    BMR.delete(0, BMR.index(customtkinter.END)-1)
    BMR.insert(0, bmr)

frame = customtkinter.CTkFrame(master=root)
frame.pack(pady=20, padx=50, fill="both", expand=True)

label = customtkinter.CTkLabel(master=frame, text="BMR Calculator", font=("Times",16,"bold"))
label.pack(pady=10, padx=5)

height = customtkinter.CTkEntry(frame, placeholder_text="Enter your height (cm)", justify="center")
height.pack(pady=10, padx=5)

weight = customtkinter.CTkEntry(frame, placeholder_text="Enter your weight (kg)", justify="center")
weight.pack(pady=10, padx=5)

age = customtkinter.CTkEntry(frame, placeholder_text="Enter your age", justify="center")
age.pack(pady=10, padx=5)

gender_menu = customtkinter.CTkOptionMenu(frame, values=["Male", "Female"], button_hover_color="green", command=gen_callback)
gender_menu.pack(pady=10, padx=5)
gender_menu.set("Gender")

button = customtkinter.CTkButton(frame, text="Calculate", command=lambda:calculate(gender_menu.get()))
button.pack(pady=5, side="bottom")

BMR = customtkinter.CTkEntry(frame, placeholder_text="Calculating...", justify="center")
BMR.pack(pady=10)


root.mainloop()