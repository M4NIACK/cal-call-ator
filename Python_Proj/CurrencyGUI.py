import customtkinter
from forex_python.converter import CurrencyRates
cr = CurrencyRates()

customtkinter.set_appearance_mode("system")
customtkinter.set_default_color_theme("dark-blue")

root = customtkinter.CTk()
root.geometry("350x420")

def conv(from_cr, to_cr):

    money = float(amount.get())
    output = cr.convert(from_cr, to_cr, money)
    output = round(output, 2)
    conv_label.configure(text=f'Converting from {from_cr} to {to_cr}:')
    result.delete(0, result.index(customtkinter.END)-1)
    result.insert(0, output)

def from_callback(selection):

    from_value = selection

def to_callback(selection):

    to_value = selection

root.title("Currency Converter")
frame = customtkinter.CTkFrame(master=root)
frame.pack(pady=20, padx=60, fill="both", expand=True)

label = customtkinter.CTkLabel(master=frame, text="Currency Converter")
label.pack(pady=10, padx=10)


amount = customtkinter.CTkEntry(master=frame, placeholder_text="Amount", justify="center")
amount.pack(pady=7, padx=10)

label_from = customtkinter.CTkLabel(frame, text = "Convert From: ")
label_from.pack()

option_from = customtkinter.CTkOptionMenu(frame, button_hover_color="green",values=["EUR", "USD", "GBP", "IDR", "BGN", "ILS", "DKK", "CAD", "JPY", "HUF", "RON", "MYR", "SEK", "SGD", "HKD", "AUD", "CHF", "KRW", "CNY", "TRY", "HRK", "NZD", "THB", "NOK", "RUB", "MXN", "CZK", "BRL", "PLN", "PHP", "ZAR"], command=from_callback)
option_from.pack(pady=6, padx=10)
option_from.set("Convert From")

label_to = customtkinter.CTkLabel(frame, text="Convert To: ").pack()
option_to = customtkinter.CTkOptionMenu(frame, button_hover_color="green", values=["EUR", "USD", "GBP", "IDR", "BGN", "ILS", "DKK", "CAD", "JPY", "HUF", "RON", "MYR", "SEK", "SGD", "HKD", "AUD", "CHF", "KRW", "CNY", "TRY", "HRK", "NZD", "THB", "NOK", "RUB", "MXN", "CZK", "BRL", "PLN", "PHP", "ZAR"], command=to_callback)
option_to.pack(pady=6, padx=10)
option_to.set("Convert To")

button = customtkinter.CTkButton(frame, text="Convert", command= lambda:conv(option_from.get(), option_to.get()))
button.pack(side="bottom")

conv_label = customtkinter.CTkLabel(frame, text="", font=("Arial", 16))
conv_label.pack()

result = customtkinter.CTkEntry(frame, placeholder_text="Converting...", justify="center")
result.pack()


root.mainloop()