import customtkinter

customtkinter.set_appearance_mode("system")
customtkinter.set_default_color_theme("dark-blue")
root = customtkinter.CTk()
root.geometry("400x420")

def conv(from_weight, to_weight):
    result.delete(0, result.index(customtkinter.END)-1)
    conv_label.configure(text=f'Converting from {from_weight} to {to_weight}:')
    weight = float(Weight.get())
# From Kilogram
    if from_weight == 'Kilogram':
        if to_weight == 'Gram':
            weight = weight * 1000
        elif to_weight == 'Milligram':
            weight = weight * 1000000
        elif to_weight == 'Pound':
            weight = weight * 2.20462
        elif to_weight == 'Ounce':
            weight = weight * 35.274
    
    elif from_weight == 'Gram':
        if to_weight == 'Kilogram':
            weight = weight / 1000
        elif to_weight == 'Milligram':
            weight = weight * 1000
        elif to_weight == 'Pound':
            weight = weight / 453.592
        elif to_weight == 'Ounce':
            weight = weight / 28.3495

    elif from_weight == 'Milligram':
        if to_weight == 'Kilogram':
            weight = weight / 1000000
        elif to_weight == 'Gram':
            weight = weight / 1000
        elif to_weight == 'Pound':
            weight = weight / 453592
        elif to_weight == 'Ounce':
            weight = weight / 28349.5
    
    elif from_weight == 'Pound':
        if to_weight == 'Kilogram':
            weight = weight / 2.20462
        elif to_weight == 'Gram':
            weight = weight * 453.592
        elif to_weight == 'Milligram':
            weight = weight * 453592
        elif to_weight == 'Ounce':
            weight = weight * 16
    
    elif from_weight == 'Ounce':
        if to_weight == 'Kilogram':
            weight = weight / 35.274
        elif to_weight == 'Gram':
            weight = weight * 28.3495
        elif to_weight == 'Milligram':
            weight = weight * 28349.5
        elif to_weight == 'Pound':
            weight = weight / 16
        
    result.insert(0, weight)

def from_callback(selection):
    from_value = selection

def to_callback(selection):
    to_value = selection

root.title("Weight Units Converter")
frame = customtkinter.CTkFrame(master=root)
frame.pack(pady=20, padx=60, fill="both", expand=True)

label = customtkinter.CTkLabel(master=frame, text="Weight Converter")
label.pack(pady=10, padx=10)

Weight = customtkinter.CTkEntry(master=frame, placeholder_text="Weight", justify="center")
Weight.pack(pady=7, padx=10)

label_from = customtkinter.CTkLabel(frame, text = "Convert From: ")
label_from.pack()

option_from = customtkinter.CTkOptionMenu(frame, button_hover_color="green",values=['Kilogram', 'Gram', 'Milligram', 'Pound', 'Ounce'], command=from_callback)
option_from.pack(pady=6, padx=10)
option_from.set("Convert From")

label_to = customtkinter.CTkLabel(frame, text="Convert To: ").pack()

option_to = customtkinter.CTkOptionMenu(frame, button_hover_color="green", values=['Kilogram', 'Gram', 'Milligram', 'Pound', 'Ounce'], command=to_callback)
option_to.pack(pady=6, padx=10)
option_to.set("Convert To")

button = customtkinter.CTkButton(frame, text="Convert", command= lambda:conv(option_from.get(), option_to.get()))
button.pack(side="bottom")

conv_label = customtkinter.CTkLabel(frame, text="", font=("Arial", 16))
conv_label.pack()

result = customtkinter.CTkEntry(frame, placeholder_text="Converting...", justify="center")
result.pack()

root.mainloop()